#!/usr/local/bin/bash


#VARS
MYSQLDUMP="/usr/local/bin/mysqldump"
MYSQLSHOW="/usr/local/bin/mysqlshow"
MYSQLDUMPOPTS="--single-transaction -Q --add-drop-database"
MYSQLAUTH="-u root -pXxxxxxx"
TOUCHBIN="/usr/bin/touch"
CUT="/usr/bin/cut"
CAT="/bin/cat"
SED="/usr/bin/sed"
GREP="/usr/bin/grep"
LOGFILE="/tmp/backuplog"
LOGERR="/tmp/backuperr"
MOUNT="/sbin/mount_nfs"
UMOUNT="/sbin/umount"
MOUNTOPTS="x.x.x.x:/raid0/data/_NAS_NFS_Exports_/NFS01 /mnt/nfs"
RSYNCBIN="/usr/local/bin/rsync"
CDATE=`date +%Y-%m-%d`
ECHO="/bin/echo"
RM="/bin/rm"
WDATE=`date +%A`
TAR="/usr/bin/tar"
MAIL="/usr/bin/mail"
MKDIR="/bin/mkdir"
GZIP="/usr/bin/gzip"
FIND="/usr/bin/find"



#CREATE LOG FILES
${ECHO} "To: xxx@energomera.ru\nFrom: yyy@energomera.ru\nSubject: WWW BACKUP SCRIPT LOG\n" >> ${LOGFILE}


# IO redirection for logging.
exec 6>&1         # Link file descriptor #6 with stdout. Saves stdout.
exec >> ${LOGFILE} # stdout replaced with file ${LOGFILE}.
exec 7>&2         # Link file descriptor #7 with stderr. Saves stderr.
exec 2>> ${LOGFILE} # stderr replaced with file ${LOGERR}.
		    


#MOUNT NFS SHARE
if ! ${MOUNT} ${MOUNTOPTS} ; then
    ${ECHO} "Error mounting destination drive, exiting..." 
    ${CAT} ${LOGFILE} | ${MAIL}  max@energomera.ru
    ${RM} ${LOGFILE} 	
    exit 1
    
fi


makersync (){
    ${ECHO} "APACHE SYNC\n" >> ${LOGFILE}
    ${RSYNCBIN} -rlDptuvW --exclude {'logs/*.log*','*.bin',} --delete  --backup --backup-dir=/mnt/nfs/backups/s01-www02/apache/rsync_archive/${CDATE} /usr/local/www/ /mnt/nfs/backups/s01-www02/apache/rsync/
    ${ECHO} "\n\nMYSQL SYNC\n" >> ${LOGFILE}
    ${RSYNCBIN} -rlDptuvW  /var/db/mysql/ /mnt/nfs/backups/s01-www02/mysql/rsync/
}


makemysqldump (){
    ${MKDIR} /mnt/nfs/backups/s01-www02/mysql/dump/${CDATE};
    for i in `${MYSQLSHOW} ${MYSQLAUTH} | ${CUT} -d "|" -f 2 | ${GREP} -v "+" | ${GREP} -v Databases | ${SED} -e 's/^ //g'`; do
    ${ECHO} "dumping DB $i ....";   
    ${MYSQLDUMP} ${MYSQLDUMPOPTS} ${MYSQLAUTH} $i > /mnt/nfs/backups/s01-www02/mysql/dump/${CDATE}/$i.sql
    done;
}


maketar (){
${TAR} -c -z  --exclude "*log\.*" -f /mnt/nfs/backups/s01-www02/apache/archives/${CDATE}.tar.gz /usr/local/www
}

makeclean (){
    ${FIND} /mnt/nfs/backups/s01-www02/mysql/dump/ -type d -mtime +10 -prune -exec ${RM} -rf "{}" \;
    ${FIND} /mnt/nfs/backups/s01-www02/apache/archives/ -type f -mtime +30 -exec ${ECHO} "Removing File => {}" \; -exec ${RM} "{}" \;
}


if [ ${WDATE} = "Friday" ] ; then 
    ${ECHO} "++++++++++++++++++"
    ${ECHO} "Friday archiving of www...."
    ${ECHO} "++++++++++++++++++"
    maketar
fi

${ECHO} "++++++++++++++++++"
${ECHO} "Make rsync..."
${ECHO} "++++++++++++++++++"
makersync

${ECHO} "++++++++++++++++++"
${ECHO} "Make mysqldump..."
${ECHO} "++++++++++++++++++"
makemysqldump


${ECHO} "++++++++++++++++++"
${ECHO} "Make clean if necessary..."
${ECHO} "++++++++++++++++++"
makeclean

#Clean up IO redirection
exec 1>&6 6>&-      # Restore stdout and close file descriptor #6.
exec 2>&7 7>&-      # Restore stdout and close file descriptor #7.


${CAT} ${LOGFILE} | ${MAIL}  xxx@energomera.ru 	

${UMOUNT} /mnt/nfs
${RM} ${LOGFILE}

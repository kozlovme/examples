FROM php:7.4-fpm AS builder

ENV PHP_OPENSSL=yes

RUN apt-get update \
    && apt-get install -y \
    libmagickwand-dev \
    libmagickcore-dev \
    libzip-dev \
    libwebp-dev

RUN pecl install imagick \
    && pecl install xdebug-3.1.0 \
    && docker-php-ext-install bcmath \
    	exif \
    	filter \
    	intl \
    	xml \
    	zip \
        pdo_mysql \
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
    && docker-php-ext-install gd


FROM php:7.4-fpm

WORKDIR /var/www

COPY --from=builder /usr/local/lib/php/extensions/no-debug-non-zts-20190902/ \
    /usr/local/lib/php/extensions/no-debug-non-zts-20190902/

RUN apt-get update \
    && apt install -y libmagickcore-6.q16-6 libmagickwand-6.q16-6 libzip4 \
    && docker-php-ext-enable bcmath \
    	exif \
    	gd \
    	imagick \
    	intl \
    	zip \
        pdo_mysql \
	xdebug \
    && rm -rf /var/lib/apt/lists/*
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

CMD ["php-fpm"]

terraform {
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
      version = "2.9.11"
    }
  }
}
provider "proxmox" {
  pm_api_url = "https://x.x.x.x:8006/api2/json"
  pm_api_token_id = "xxxx@pam!terraform_api_token"
  pm_api_token_secret = "35f64a"
  pm_tls_insecure = true
  pm_log_enable = true
  pm_log_file   = "terraform-plugin-proxmox.log"
  pm_debug      = true
  pm_log_levels = {
    _default    = "debug"
    _capturelog = ""
  }

}

resource "proxmox_vm_qemu" "debian_server" {
  count = 1 # just want 1 for now, set to 0 and apply to destroy VM
  name = "s01-web06"
  target_node = var.proxmox_host
  clone = var.template_name
  agent = 1
  os_type = "cloud-init"
  cores = 2
  sockets = 1
  cpu = "host"
  memory = 4096
  scsihw = "virtio-scsi-pci"
  bootdisk = "scsi0"
  disk {
    slot = 0
    size = "20G"
    type = "scsi"
    storage = "vmpool1"
  }
  
  network {
    model = "virtio"
    bridge = "vmbr1"
  }
  lifecycle {
    ignore_changes = [
      network,
    ]
  }
  

  ipconfig0 = "ip=x.x.x.x/21,gw=x.x.x.1"

  connection {
    type = "ssh"
    user = "debian"
    private_key = file(pathexpand("~/.ssh/id_rsa"))
    host = "x.x.x.x" 
  } 

  
  provisioner "remote-exec" {
    inline = [
        "sudo adduser --disabled-password --gecos \"\" devops",
        "sudo mkdir -p /home/devops/.ssh",
        "sudo cp /home/debian/.ssh/authorized_keys /home/devops/.ssh/authorized_keys",
        "sudo chown -R devops /home/devops/.ssh",
        "sudo chmod 700 /home/devops/.ssh",
        "sudo chmod 600 /home/devops/.ssh/authorized_keys",
        "sudo usermod -aG sudo devops",
        "echo \"devops ALL=(ALL) NOPASSWD:ALL\" | sudo tee /etc/sudoers.d/devops",
        "echo \"nameserver x.x.x.x\" | sudo tee /etc/resolv.conf"
    ]
  }

  sshkeys = <<EOF
  ${var.ssh_key}
  EOF
}
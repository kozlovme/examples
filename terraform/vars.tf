variable "ssh_key" {
  default = "ssh-rsa werewrwerwerwerwer8RTNB/pG7MTyOVM9r8tF6a5Le7Zx0= devops@s01-op01"
}
variable "proxmox_host" {
    default = "s01-pve01"
}
variable "template_name" {
    default = "debian-11-cloudinit-template"
}
